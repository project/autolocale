<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function localized_profile_modules() {
  return array('color', 'comment', 'help', 'taxonomy', 'dblog', 'autolocale');
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function localized_profile_details() {
  return array(
    'name' => 'Drupal localized',
    'description' => 'Localized interface (will be integrated to default profile).'
  );
}

/**
 * Return a list of tasks that this profile supports.
 *
 * @return
 *   A keyed array of tasks the profile will perform during the _final stage.
 */
function localized_profile_task_list() {
  global $install_locale;
  if (!empty($install_locale) && ($install_locale != 'en')) {
    return array('localeimport' => st('Import translation'));
  }
  return array();
}

/**
 * Final tasks to import translation and create node types properly.
 */
function localized_profile_final(&$task) {
  global $profile, $install_locale, $base_url, $user;
  
  // Insert default user-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  // Insert default user-defined node types into the database.
  $common = array(
    'module' => 'node',
    'custom' => TRUE,
    'modified' => TRUE,
    'locked' => FALSE,
    'has_body' => TRUE,
    'body_label' => st('Body'),
    'has_title' => TRUE,
    'title_label' => st('Title'),
    'help' => '',
    'min_word_count' => '',
  );
  $types = array(
    array_merge(
      array(
        'type' => 'page',
        'name' => st('Page'),
        'description' => st('If you want to add a static page, like a contact page or an about page, use a page.')
      ), 
      $common
    ),
    array_merge(
      array(
        'type' => 'story',
        'name' => st('Story'),
        'description' => st('Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.')
      ),
      $common
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);

  // Determine if another language was enabled during installation.
  // Enable it, import translations using a batch import.
  if (!empty($install_locale) && ($install_locale != 'en') && $task == 'configure') {
    include_once drupal_get_path('module', 'autolocale') .'/autolocale.module';
    include_once drupal_get_path('module', 'autolocale') .'/autolocale.install';
    
    // The user is logged in, but has no session ID yet, which
    // would be required to be the same throughout the batch here.
    $user->sid = session_id();

    // Enable installation language as default site language.
    _autolocale_install_language($install_locale);
    // Create batch
    $batch = autolocale_batch_import($install_locale);
    if (!empty($batch)) {
      // We need to ensure batch path exists
      menu_rebuild();
      // Start a batch, switch to localeimport state.
      variable_set('install_task', 'localeimport');
      batch_set($batch);
      $path = $base_url .'/install.php?locale='. $install_locale .'&profile='. $profile;
      batch_process($path, $path);
    }
    else {
      // Installation done, because found nothing to import.
      $task = 'finished';
      return '';
    }
  }
  // Localeimport task is about importing all files.
  if ($task == 'localeimport'){
    include_once 'includes/batch.inc';
    return _batch_page();
  }
}

